# NPD - Now Player DelamusiquequejoueMPD

Application permettant d'afficher ce que joue MPD dans une page web, utiliser comme layout pour OBS.

![](screen.jpg)

## Dépendance

* Python
* aiohttp
* aiohttp_jinja2
* D'un navigateur web
