console.log('Hello World!');

let ws = 'ws://' + host + ":" + port + '/ws'
console.log(ws)
let sock = new WebSocket(ws);

sock.onmessage = function (event) {
        data = JSON.parse(event.data)
        if ( data.event == "syn" ) {
                console.log("ACK-SYN");
                display(data)
                sock.send("status");
        } else if (data.event == "player") {
                console.log("Player");
                display(data)
                sock.send("status");
        } else {
                console.log("Else");
                sock.send("status");
        }
};

sock.onopen = function(){
        sock.send("syn");
}

function display(data) {
        if (!data.display) {
                items = document.getElementsByClassName("mpd")
                for (let item of items) {
			if ( item.classList.contains("display")) {
				item.classList.remove("display")
				item.classList.add("no_display")
			}
                }
        } else {
                items = document.getElementsByClassName("mpd")
                for (let item of items) {
                        item.classList.remove("no_display")
                        item.classList.add("display")
                }
                if ( data.data.title === undefined ) {
			data.data.title = data.data.file.split("/").slice(-1)
                }
                document.getElementById("title").innerHTML = data.data.title;
                document.getElementById("artist").innerHTML = data.data.artist;
                document.getElementById("album").innerHTML = data.data.album;
        }
}
