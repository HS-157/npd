import aiohttp_jinja2
import asyncio
import jinja2
import json

import aiohttp.web

from mpd.asyncio import MPDClient
from pprint import pprint

HOST = 'localhost'
PORT = 8080

async def websocket_handler(request):
    last_state = None
    print('Websocket connection starting')
    ws = aiohttp.web.WebSocketResponse()
    await ws.prepare(request)
    print('Websocket connection ready')

    print("MPD connection starting")
    client = MPDClient()
    client.timeout = 10
    client.idletimeout = None
    await client.connect("localhost", 6600)
    print("MPD connection ready")

    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
            print("Client : %s" % msg.data)
            if msg.data == 'close':
                await ws.close()
            elif msg.data == "syn":
                display = False
                print("Command : SYN")
                status = await client.status()
                if status.get("state") == "play":
                    display = True
                    song = await client.currentsong()
                else:
                    display = False
                    song = None
                syn_anwser = {
                        "event": "syn",
                        "display": display,
                        "state": status.get("state"),
                        "data": song
                        }
                await ws.send_str(json.dumps(syn_anwser))
            elif msg.data == "status":
                print("Command : STATUS")
                await status_mpd(ws, client)
            else:
                print("Command : %s" % msg.data)
                await ws.send_str(msg.data)

    print('Websocket connection closed')
    client.close()
    client.disconnect()
    print("MPD connection closed")
    return ws


@aiohttp_jinja2.template("mpd.html")
async def mpd(request):
    r = {
            "host": HOST,
            "port": PORT
            }
    return r

@aiohttp_jinja2.template("intermission.html")
async def intermission(request):
    title = request.rel_url.query.get('title', None)
    title2 = request.rel_url.query.get('title2', None)
    r = {
            "host": HOST,
            "port": PORT,
            "title": title,
            "title2": title2
            }
    pprint(r)
    return r


async def status_mpd(ws, client):
    print("===" * 10)
    display = False
    print("Idle")
    async def iter_idle(client):
        async for i in client.idle():
            print("Iter idle !")
            yield i

    async for idle in iter_idle(client):
        print("> %s" % idle)

        if "player" in idle:
            print("Player !")
            status = await client.status()
            state = status.get("state")
            if state == "play":
                print("State : Play")
                display = True
                song = await client.currentsong()
            else:
                print("State : Pause / Stop")
                display = False
                song = None
            anwser = {
                    "event": "player",
                    "state": state,
                    "display": display,
                    "data": song
                    }
            await ws.send_str(json.dumps(anwser))
        else:
            anwser = {
                    "event": idle,
                    "display": display
                    }
            await ws.send_str(json.dumps(anwser))
            print("Anwser !")
            print("===" * 10)


def main():
    loop = asyncio.get_event_loop()
    app = aiohttp.web.Application()
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('./template'))
    app.router.add_route('GET', '/mpd', mpd)
    app.router.add_route('GET', '/intermission', intermission)
    app.router.add_route('GET', '/ws', websocket_handler)
    app.router.add_static('/static/', path='static', show_index=True)

    aiohttp.web.run_app(app, host=HOST, port=PORT)


if __name__ == '__main__':
    main()
